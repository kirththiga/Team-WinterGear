#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "listofstrings.c"

FILE *fp, *fw, *fh, *fh2;
int filesize, readsize;

int main(int argc, char* argv[])
{		
	printf("Content-type: text/html\n\n");
	int n = atoi(getenv("CONTENT_LENGTH"));
	int a = 0;
	int i = 5;
	int j = 0;
	char string[200];
	char c;
	char inputUser[100];
	char inputPassword[100];
	int check = 0;
	
// BEGIN READ FORM DATA	
	// Reading the user's input and storing it in string
	while((c = getchar()) != EOF && a<n) {
		if(a < 200) {
			string[a] = c;
		}
		a++;
	}
	string[a]= '\0';
	
	// Obtaining the username from the string
	while(string[i] != '&'){
		inputUser[i-5] = string[i];
		i++;
	}
	inputUser[i-5] = '\0';
	j = strlen(inputUser);
	i = 5+j+6;
	
	// Obtaining the password from the string
	for(i=5+j+6; i<n; i++){
		inputPassword[i-(j+11)] = string[i];
	}
	inputPassword[i-(j+11)] = '\0';

// END READ FORM DATA
	
	//Open file
	fp = fopen("Members.csv", "rt");
	
	if(fp == NULL){
		printf("Can't open Members.csv");
	}
	
	//Obtain size in bytes of file. This is the maximum length of a line (limit case: file has one line)
	fseek(fp, 0, SEEK_END);
	filesize = ftell(fp);
	rewind(fp);

	char* buffer;
	buffer = (char*)malloc(sizeof(char) * (filesize + 1));
	readsize = fread(buffer, sizeof(char), filesize, fp);
	buffer[filesize] = '\0';

	const char delimiter[] = "\n";

	char *token;
	
	// A linked list is created to store the strings (rows) from the text file
	aNode* list = (aNode*)malloc(sizeof(aNode));
	
	//The FIRST LINE IS COPIED INTO THE LIST
	token = strtok(buffer, delimiter);
	list = add(list,token);
	
	//THE REMAINING LINES, IF ANY, ARE ADDED TO THE LIST
	while ((token = strtok(NULL, delimiter)) != NULL){
		
		char *copy = malloc(strlen(token) + 1); 
		strcpy(copy, token);
		
		list = add(list,copy);
	}
	
	char* username;
	char* password;
	char* fullname;
	
	char line[200];
	
	while(list->next != NULL) {
		strcpy(line, list->next->value);
		
		fullname = strtok(line, ",");
		username = strtok(NULL, ",");
		password = strtok(NULL, ",");
		
		// Check if the username and password from csv file matches the user's input
		if( (strcmp(username, inputUser) == 0) && (strcmp(password, inputPassword) == 0)){
			// Check is set to 1 if the authentication is valid.
			check = 1;
			//Open file and append the loggedin user's username
			fw = fopen("LoggedIn.csv", "at");
			if(fw == NULL){
				printf("Can't open LoggedIn.csv");
			}
			fprintf(fw, "%s\n", inputUser);
			
			// Open files
			fh = fopen("part1.txt", "rt");
			if(fh == NULL){
				printf("Can't open part1.txt");
			}
	
			fh2 = fopen("part2.txt", "rt");
			if(fh2 == NULL){
				printf("Can't open part2.txt");
			}
	
			// Read and print all the lines from the first file
			if(fh) {
				char buffer[4096];

				while(fgets(buffer, sizeof(buffer),fh) != 0){
					fputs(buffer, stdout);
				}
			}
			
			// Insert a line, then read and print all the lines from the second file
			if(fh2) {
				char buffer2[4096];
				
				printf("%s",inputUser);
			
				while(fgets(buffer2, sizeof(buffer2),fh2) != 0){
					fputs(buffer2, stdout);
				}	
				
			}
	
			break;
		}

		list = list->next;
	} 

	// Error HTML Page when authentication is invalid
	if(check == 0) {
		printf("<body bgcolor=\"#F5F1DE\" text=\"black\">");
		printf("<h1 align=\"center\">Team Winter Gear</h1><br/><br/>");
		printf("<h2 style=\"color:red\"> Error </h2><br/>");
		printf("<p>Go back to <a href=\"../login.html\"><b>LOGIN</b></a></p>");
		printf("</body>");
		exit(0);
	}
	// Closing the files
	fclose(fp);
	fclose(fw);
	fclose(fh);
	fclose(fh2);
	
	return 0;
}

