#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct str_node
{
	char* value;
	struct str_node* next;
} aNode;

aNode* head = NULL;

//function that takes a string pointer (char*) and adds it to the specified linked list
aNode* add(aNode* list, char* value)
{
	aNode* freeSpot;
	aNode* newNode;
	
	//Find the last node in the chain
	freeSpot = list;
	while(freeSpot->next != NULL)
	{
		freeSpot = freeSpot->next;
	}
	
	//Create a new node using malloc
	newNode = (aNode*)malloc(sizeof(aNode));
	newNode->value = value;
	newNode->next = NULL;
	
	//Link new node to existing chain.
	freeSpot->next = newNode;
	
	return list;
}

void printlist(aNode* list2print)
{
	while(list2print->next != NULL)
	{
		printf("%s\n",list2print->next->value);
		list2print = list2print->next;
	}
}