#!/usr/bin/perl -w
use CGI qw(:standard);

#Function definition for the CSV handler
# USAGE: save (username, fullname, password)
sub save
{
	# Define location of CSV and open file
	my $file = "Members.csv";
	open (my $fh, "<", $file) or die "$file: $!";

	# Define search target (username)
	my $target = $_[0];
	chomp($target);

	# Split each record of the CSV into arrays
	my @fields;
	my $found = 0;
	while (my $line = <$fh>)
	{
		#chomp $line;
		my @fields = split ("," , $line);
		
		# perform string search
		if (@fields->[1] eq $target)
		{
			$found = 1;
		}
	}

	close $fh;

	open (my $append, ">>", $file) or die "$file: $!";

	if ($found == 0)
	{
		print $append "$_[1],$_[0],$_[2]\n";
		print "<p style='color:green'>Success! User <i>$_[0]</i> has been created</p>";
		print '<p><a href="../register.html">Register another user</a></p>';
	}
	else
	{
		print "<p style='color:red'>Sorry, username <i>$_[0]</i> is already taken. Please try again.</p>";
		print '<p><a href="../register.html">Back to Registration page</a></p>';
	}
}

## MAIN FUNCTION STARTS HERE
# Static output to browser
print "Content-type: text/html\n\n";
print <<EOF;
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<title>Registration Page</title>
	<meta name="description" content="Welcome to the Cool New Website!!" />
	<meta name="keywords" content="perl,COMP206,assignment4" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="en-us" />
	<link rel="stylesheet" type="text/css" href="../style.css"/>
</head>

<body bgcolor="#F5F1DE" text="black">

<h1 align="center">Team Winter Gear
	<object type="application/x-shockwave-flash"
		data="bgsound/musicplayer.swf?&playlist_url=bgsound/playlist.xspf" 
		width="17" height="17" style="float: right"><param name="movie" 
		value="bgsound/musicplayer.swf?&playlist_url=bgsound/playlist.xspf" />
		<img src="noflash.gif" 
		width="17" height="17" alt="Music Player" />
	</object>
	</h1>
	<table width="980">
		<tr bgcolor="#096BB2">
			<td colspan="2">
				<ul>
					<li><a href="../home.html">HOME</a></li>
					<li><a href="../catalogue.html">CATALOGUE</a></li>
					<li><a href="../login.html" target="_blank">LOGIN</a></li>
					<li><a href="../register.html">REGISTRATION</a></li>
				</ul>
			</td>
		</tr>
	</table>

	<h2>Registration</h2>

EOF

# Retrieve form data
$user = param('username');
$name = param('fullname');
$pass = param('pw');
$pass2 = param('cpw');

# Check that all fields have been filled
if ( ($user eq "") || ($name eq "") || ($pass eq "") )
{
	print "<p style='color:red'>Error - make sure to correctly fill in all fields. Please try again.</p>\n";
	print '<p><a href="../register.html">Back to Registration page</a></p>';
}
else
{
	if ($pass ne $pass2)
	{
		print "<p style='color:red'>Passwords do not match! Please try again.</p>\n";
		print '<p><a href="../register.html">Back to Registration page</a></p>';
	}
	else
	{
		#send username, name, and password to the SAVE function above
		save($user,$name,$pass);
	}
}

# Terminate HTML output
print <<EOF;

	</body>
	</html>
	
EOF
