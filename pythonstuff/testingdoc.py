import csv
import os


#function for getting the filepath of a file
def get_file_path(filename):
	currentdirpath = os.getcwd
	file_path = os.path.join(os.getcwd(), filename)
	print file_path
	return file_path


#storing the fields into variable
path = get_file_path('Inventory.csv')
out  = open(path, 'r')
data = csv.reader(out)

#reading from the csv file
dataToList = list(data)
numberOfCoatsFromData = dataToList[0][1]
numberOfGlovesFromData = dataToList[1][1]
numberOfScarvesFromData = dataToList[2][1]


#reading the price of items
priceOfCoats = dataToList[0][2]
priceOfGloves = dataToList[1][2]
priceOfScarves = dataToList[2][2]


#puts the changes into a list
changes = [
['coats', 98, 500],
['gloves', 98, 500],
['scarves', 98, 500]
]

#overwrites the csv file
with open(path, 'w') as f:
	writer = csv.writer(f)
	writer.writerows(changes)


