#!/usr/bin/python

import os
import csv
import random
import string
import cgi



print "Content-type:text/html\n"


#Displaying the usual menu in html if logged in
#Here I should verify the hidden field with cgi form

lol = "just testing"
loggedIn = True



#i need to update this when using kirth's file
form = cgi.FieldStorage()

coatsBought = form.getvalue('numberCoats')
glovesBought = form.getvalue('numberGloves')
scarvesBought = form.getvalue('numberScarves')


spentOnCoats = int(coatsBought)
spentOnCoats = spentOnCoats*500

spentOnGloves = int(glovesBought)
spentOnGloves = spentOnGloves*250

spentOnScarves = int(scarvesBought)
spentOnScarves = spentOnScarves*150

total = spentOnCoats + spentOnGloves + spentOnScarves




if(loggedIn):

	print '''

		<html>

		<head>

		<meta charset="UTF-8">

		<title> Home Page </title>

		<link rel="stylesheet" type="text/css" href="style.css"/>

		</head>

		<body bgcolor="#F5F1DE" text="black">

		<h1 align="center">Team Winter Gear</h1>

		<table width="980">

			<tr bgcolor="#096BB2">

				<td colspan="3">

					<ul>
						<li><a href="home.html">HOME</a></li>
						<li><a href="catalogue.html">CATALOGUE</a></li>
						<li><a href="login.html" target="_blank">LOGIN</a></li>
					</ul>

				</td>

			</tr>

		</table>

		</br>
		</br>

		<h4>
			
			<p> Here is your bill: </p>


			<p> Coats: %i </p>

			<p> Gloves: %i </p>

			<p> Scarves: %i </p>

			<p> Total: %i </p>

			</br>
			<a href="home.html">HOME</a>
			</br>
			<a href="catalogue.html">CATALOGUE</a>

		</h4>
		
		</body>

		</html>

		''' % (spentOnCoats, spentOnGloves, spentOnScarves, total)


if(loggedIn is False):

	print '''

		<h1> ERROR </h1>
		
		<a href="catalogue.html">CATALOGUE</a>

		'''
#Gotta remember to put here a link for people to go back to catalogue























#function for getting the filepath of a file
def get_file_path(filename):
	currentdirpath = os.getcwd
	file_path = os.path.join(os.getcwd(), filename)
	return file_path

#generic reader for testing
def read_csv(filepath):
	with open(filepath, 'rU') as csvfile:
		reader = csv.reader(csvfile)
		for row in reader:
			print row
			#print row[0], row[1], row[2] 
			#



#storing the fields into variable
path = get_file_path('Inventory.csv')
out  = open(path, 'r')
data = csv.reader(out)

#reading from the csv file
dataToList = list(data)
numberOfCoatsFromData = dataToList[0][1]
numberOfGlovesFromData = dataToList[1][1]
numberOfScarvesFromData = dataToList[2][1]


#reading the price of items
priceOfCoats = dataToList[0][2]
priceOfGloves = dataToList[1][2]
priceOfScarves = dataToList[2][2]



subtractedCoats = numberOfCoatsFromData - (spentOnCoats/500)
subtractedGloves = numberOfGlovesFromData - (spentOnGloves/250)
subtractedScarves = numberOfScarvesFromData - (spentOnScarves/150)

#puts the changes into a list
changes = [
['coats', subtractedCoats, priceOfCoats],
['gloves', subtractedGloves, priceOfGloves],
['scarves', subtractedScarves, priceOfScarves]
]

def write_csv(self, param1, param2, param3):
  f = open(self.fileName, 'a')
  data = param1 + "," + param2 + "," + param3 + "\n";
  f.write(data)
  f.close()


'''
#overwrites the csv file
with open(path, 'w') as f:
	writer = csv.writer(f)
	writer.writerows(changes)

'''